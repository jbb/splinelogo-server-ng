{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        crossSystem = {
          config = "armv6l-unknown-linux-musleabihf";
        };
        pkgs = import nixpkgs {
          inherit system overlays crossSystem;
        };
      in
      {
        devShell = pkgs.callPackage
          (
            { mkShell, gnumake, rust-bin }:
            mkShell {
              nativeBuildInputs = [
                gnumake
                (
                  rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
                    extensions = [ "rust-src" "rust-std" "cargo" ];
                    targets = [ "arm-unknown-linux-musleabihf" ];
                  })

                )
              ];
            }
          )
          { };



        /*
          pkgs.mkShell
          {
          # Empty, because we don't need anything on the target
          buildInputs = [ ];
          # Everything we need at build time, for the HOST architecture
          depsXXXBuild = [ pkgs.gcc ];


          };
        */
      });
}
