const ENDPOINT_URL: &str = "https://iot.spline.de/api/change.php";

pub fn report_open() {
    let key = std::env::var("SPACEAPI_TOKEN").expect(
        "Failed to read Space API token. Did you set the SPACEAPI_TOKEN environment variable?",
    );
    ureq::post(ENDPOINT_URL)
        .send_form(&[(&"apiKey", &key), (&"doorOpen", &"true")])
        .unwrap();
}

pub fn report_closed() {
    let key = std::env::var("SPACEAPI_TOKEN").expect(
        "Failed to read Space API token. Did you set the SPACEAPI_TOKEN environment variable?",
    );
    ureq::post(ENDPOINT_URL)
        .send_form(&[(&"apiKey", &key), (&"doorOpen", &"false")])
        .unwrap();
}
