use sysfs_gpio::{Direction, Pin};

mod mattermost;
mod spaceapi;
mod telegram;
mod tick;
pub mod watcher;

const DOOR_MAGNET_PIN: u64 = 2;

#[derive(Debug, Copy, Clone)]
pub enum DoorState {
    OPEN,
    CLOSED,
}

impl DoorState {
    pub fn get() -> sysfs_gpio::Result<DoorState> {
        let magnet = Pin::new(DOOR_MAGNET_PIN);
        magnet.export()?;
        magnet.set_direction(Direction::In)?;

        match magnet.get_value()? {
            0 => return Ok(DoorState::CLOSED),
            1 => return Ok(DoorState::OPEN),
            _ => panic!("Unexpected value received from GPIO"),
        }
    }
}
