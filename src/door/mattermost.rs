const MATTERMOST_FQDN: &str = "chat.spline.de";
const DOOR_OPEN_MSG: &str = "The fucking door is open";
const DOOR_CLOSE_MSG: &str = "The door is closed";

pub fn report_open() {
    send(DOOR_OPEN_MSG);
}

pub fn report_closed() {
    send(DOOR_CLOSE_MSG);
}

fn send(text: &str) {
    let key = std::env::var("MATTERMOST_TOKEN").expect(
		"Failed to read Mattermost API token. Did you set the MATTERMOST_TOKEN environment variable?",
	);
    let url = format!("https://{}/hooks/{}", MATTERMOST_FQDN, key);
    ureq::post(&url)
        .send_json(ureq::json!({
            "text": text,
        }))
        .unwrap();
}
