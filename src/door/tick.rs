use std::process::{Command, Stdio};

const TICK_MAC_ADDR: &str = "00:1e:37:cb:45:ff";
const SSH_TARGET: &str = "dashboard@tick.spline.de";

pub fn report_open() {
    let s = Command::new("wakeonlan")
        .arg(TICK_MAC_ADDR)
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .status()
        .expect("Failed to wake tick");
    if !s.success() {
        panic!("Failed to wake tick: wakeonlan returned failed status code");
    }
}

pub fn report_closed() {
    let s = Command::new("ssh")
        .arg(SSH_TARGET)
        .arg("sudo systemctl suspend")
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .status()
        .expect("Failed to wake tick");
    if !s.success() {
        panic!("Failed to wake tick: wakeonlan returned failed status code");
    }
}
