use std::{sync::mpsc, sync::mpsc::SyncSender, thread, time};

use crate::led;

use super::mattermost;
use super::spaceapi;
use super::telegram;
use super::tick;
use super::DoorState;

pub fn start(led_communicator: SyncSender<Vec<(u8, u8, u8)>>) {
    thread::spawn(move || {
        // Initialize Telegram bot
        let tg = telegram::DoorNotifier::new();
        let mut old_state: Option<DoorState> = None;
        loop {
            // Only trigger changes if different from last door state
            let new_state = DoorState::get().unwrap();

            let (_, rx) = mpsc::channel();

            match old_state {
                // Is state the same?
                Some(s) => match s {
                    DoorState::OPEN => match new_state {
                        DoorState::OPEN => (),
                        DoorState::CLOSED => report_door_closed(led_communicator.clone(), rx, &tg),
                    },
                    DoorState::CLOSED => match new_state {
                        DoorState::CLOSED => (),
                        DoorState::OPEN => report_door_open(led_communicator.clone(), rx, &tg),
                    },
                },

                // Handle case of unknown state (just restarted) by doing nothing
                None => (),
            }

            // For now, check whether door is open once a second
            thread::sleep(1 * time::Duration::from_secs(1));
            old_state = Some(new_state)
        }
    });
}

fn report_door_open(
    led_communicator: SyncSender<Vec<(u8, u8, u8)>>,
    cancel: mpsc::Receiver<()>,
    tg: &telegram::DoorNotifier,
) {
    println!("Door has been opened");
    let pattern = led::patterns::ColorFillUninterruptable::new((0, 255, 0));
    led::start_pattern(led_communicator, Box::new(pattern), cancel);
    spaceapi::report_open();
    mattermost::report_open();
    tick::report_open();
    tg.report_open();
}

fn report_door_closed(
    led_communicator: SyncSender<Vec<(u8, u8, u8)>>,
    cancel: mpsc::Receiver<()>,
    tg: &telegram::DoorNotifier,
) {
    println!("Door has been closed");
    let pattern = led::patterns::ColorFillUninterruptable::new((255, 0, 0));
    led::start_pattern(led_communicator, Box::new(pattern), cancel);
    spaceapi::report_closed();
    mattermost::report_closed();
    tick::report_closed();
    tg.report_closed();
}
