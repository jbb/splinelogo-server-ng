use std::env;

use telbot_ureq::types::chat::ChatId;
use telbot_ureq::types::file::InputFile;
use telbot_ureq::types::message::{SendMessage, SendVideo};
use telbot_ureq::Api;

const CHAT_ID: ChatId = ChatId::Id(-1001352936285);
const DOOR_OPEN_MSG: &str = "The f***ing Door is open";
const DOOR_CLOSED_GIF: &[u8] = include_bytes!("../../gendalf.gif");
const DOOR_CLOSED_GIF_NAME: &str = "gendalf.gif";
const DOOR_CLOSED_CAPTION: &str = "The door is closed!";

pub struct DoorNotifier {
    api: Api,
}

impl DoorNotifier {
    pub fn new() -> DoorNotifier {
        let key = env::var("TELEGRAM_TOKEN").expect(
			"Failed to read Telegram bot API token. Did you set the TELEGRAM_TOKEN environment variable?"
		);
        let api = Api::new(key);

        return DoorNotifier { api };
    }

    pub fn report_open(&self) {
        let msg = SendMessage::new(CHAT_ID, DOOR_OPEN_MSG);
        self.api.send_json(&msg).unwrap();
    }

    pub fn report_closed(&self) {
        // Send actual chat message
        let photo = InputFile {
            name: String::from(DOOR_CLOSED_GIF_NAME),
            data: Vec::from(DOOR_CLOSED_GIF),
            mime: "image/gif".to_string(),
        };
        let msg = SendVideo::new(CHAT_ID, photo).with_caption(DOOR_CLOSED_CAPTION);
        self.api.send_file(&msg).unwrap();
    }
}
