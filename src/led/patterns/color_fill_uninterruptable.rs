use super::super::*;
use crate::led::constants;

pub struct ColorFillUninterruptable {
    color: RGB,
}

impl ColorFillUninterruptable {
    pub fn new(color: RGB) -> ColorFillUninterruptable {
        ColorFillUninterruptable { color }
    }
}

impl LightPattern for ColorFillUninterruptable {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        mut cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        let mut data: Vec<RGB> = Vec::new();
        for _ in 0..constants::NUM_LEDS {
            data.push(self.color);
        }
        submit_and_maybe_yield(data, &mut adapter_sender, &mut cancel)
    }
}
