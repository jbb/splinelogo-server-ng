use super::super::*;

use crate::led::constants;

pub struct ColorWiper {}

impl LightPattern for ColorWiper {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        mut cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        // TODO: Choose color, speed etc. in constructor

        let mut data: Vec<RGB> = Vec::new();
        for _ in 0..constants::NUM_LEDS {
            data.push((255, 0, 0));
        }
        submit_and_maybe_yield(data, &mut adapter_sender, &mut cancel)
    }
}
