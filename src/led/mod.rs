use std::sync::mpsc;
use std::sync::mpsc::SyncSender;

pub mod adapter;
pub use adapter::create_adapter_thread;

pub mod constants;
pub mod patterns;

type RGB = (u8, u8, u8);
type AdapterSender = SyncSender<Vec<RGB>>;
pub trait LightPattern: Sync + Send {
    fn start(
        &self,
        adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason>;
}

#[derive(Debug)]
pub enum StopReason {
    StoppedByUser,
}

// TODO: Error handling
fn submit_and_maybe_yield(
    data: Vec<RGB>,
    adapter_sender: &mut AdapterSender,
    cancel: &mut mpsc::Receiver<()>,
) -> Result<(), StopReason> {
    // Submit data to adapter
    // TODO: Proper error handling/retry
    adapter_sender.send(data).unwrap();

    // Decide whether to yield
    if cancel.try_recv().is_ok() {
        // Simply yield by terminating our thread
        return Err(StopReason::StoppedByUser);
    }

    Ok(())
}

pub fn start_pattern(
    adapter_sender: AdapterSender,
    p: Box<dyn LightPattern>,
    mut cancel: mpsc::Receiver<()>,
) {
    std::thread::spawn(move || {
        if let Err(stop_reason) = p.start(adapter_sender, &mut cancel) {
            eprintln!("Pattern stopped before it could finish: {:?}", stop_reason);
            return;
        }
    });
}
