use std::sync::mpsc;

mod door;
mod led;
mod led_tcp;

fn main() {
    println!("{:?}", door::DoorState::get().unwrap());

    // Test
    let (adapter_sender, _) = led::create_adapter_thread();
    let (_, rx) = mpsc::channel();
    led::start_pattern(
        adapter_sender.clone(),
        Box::new(led::patterns::ColorWiper {}),
        rx,
    );

    door::watcher::start(adapter_sender);

    loop {}
}
